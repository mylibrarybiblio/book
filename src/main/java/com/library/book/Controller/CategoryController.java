package com.library.book.Controller;

import com.library.book.Dto.BookDto;
import com.library.book.Entity.Category;
import com.library.book.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/addCategory")
    public ResponseEntity<Category> addCategory(@RequestBody Category category){
        categoryService.addCategory(category);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/updateCategory")
    public ResponseEntity<Category> updateCategory(@RequestBody Category category){
        categoryService.updateCategory(category);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/deleteCategory/{idCategory}")
    public ResponseEntity<Void> deleteCategory(@PathVariable int idCategory){
        categoryService.deleteCategory(idCategory);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/allCategory")
    public ResponseEntity<List<Category>> listCategory(){
        List<Category> allCategory = categoryService.getAllCategory();
        return new ResponseEntity<>(allCategory, HttpStatus.FOUND);
    }

    @GetMapping("/category/{idCategory}")
    public ResponseEntity<Category> oneCategory(@PathVariable int idCategory){
        Category category= categoryService.getOneCategory(idCategory);
        return new ResponseEntity<>(category, HttpStatus.FOUND);
    }
}
