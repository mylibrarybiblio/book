package com.library.book.Controller;

import com.library.book.Dto.BookDto;
import com.library.book.Entity.Book;
import com.library.book.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping("/addBook")
    public ResponseEntity<BookDto> addBook(@RequestBody BookDto bookDto){
        bookService.addBook(bookDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/updateBook")
    public ResponseEntity<BookDto> updateBook(@RequestBody BookDto bookDto){
        bookService.updateBook(bookDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/deleteBook/{idBook}")
    public ResponseEntity<Void> deleteBook(@PathVariable int idBook){
        bookService.deleteBook(idBook);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/allBook")
    public ResponseEntity<List<BookDto>> listBook(){
        List<BookDto> bookDtoList = bookService.getAllBooks();
        return new ResponseEntity<>(bookDtoList, HttpStatus.FOUND);
    }

    @GetMapping("/book/{idBook}")
    public BookDto oneBook(@PathVariable int idBook){
        BookDto bookDto= bookService.getOneBook(idBook);
        return bookDto;
    }

    @GetMapping("/search/{word}")
    public List<BookDto> searchBook(@PathVariable String word) {
        List<BookDto> listBookDtoSearch = bookService.searchBook(word);
        return listBookDtoSearch;
    }

}
