package com.library.book.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idBook;

    private String title;

    private String isbn;

    private Date registerDate;

    private int nbExamplaries;

    private String author;

    @ManyToOne()
    private Category category;

    public Book() {
    }

    public Book(String title, String isbn, Date registerDate, Integer nbExamplaries, String author, Category category) {
        this.title = title;
        this.isbn = isbn;
        this.registerDate = registerDate;
        this.nbExamplaries = nbExamplaries;
        this.author = author;
        this.category = category;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public int getNbExamplaries() {
        return nbExamplaries;
    }

    public void setNbExamplaries(int nbExamplaries) {
        this.nbExamplaries = nbExamplaries;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
