package com.library.book.Service;

import com.library.book.Dto.BookDto;
import com.library.book.Entity.Book;
import com.library.book.Exception.BookException;
import com.library.book.Repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public void addBook(BookDto bookDto) {
    Book bookExist = bookRepository.findByIsbn(bookDto.getIsbn());
    if (bookExist != null) throw new BookException("Ce Livre existe déjà");
    bookDto.setRegisterDate(new Date());
    Book newBook = convertToEntity(bookDto);
    bookRepository.save(newBook);
    }

    public void updateBook(BookDto bookDto) {
        Book bookExist = bookRepository.findById(bookDto.getIdBook())
                .orElseThrow(()-> new BookException("Ce livre n'existe pas"));
        Book book = convertToEntity(bookDto);
        bookRepository.save(book);
    }

    public void deleteBook(int idBook) {
        Book bookExist = bookRepository.findById(idBook).orElseThrow(()-> new BookException("Ce livre n'existe pas"));
        bookRepository.delete(bookExist);
    }

    public List<BookDto> getAllBooks() {
        List<Book> bookList = bookRepository.findAll();
        List<BookDto> bookDtoList = bookList.stream().map(this::convertToDto).collect(Collectors.toList());
        return bookDtoList;
    }

    public BookDto getOneBook(int idBook) {
        Book bookExist = bookRepository.findById(idBook).orElseThrow(()-> new BookException("Le livre avec Id "+idBook+" n'existe pas"));
        BookDto bookDto = convertToDto(bookExist);
        return bookDto;
    }

    public List<BookDto> searchBook(String word) {
        List<Book> listBookSearchTitleAndIsbn = bookRepository.findByTitleContains(word);
        listBookSearchTitleAndIsbn.add(bookRepository.findByIsbn(word));
        listBookSearchTitleAndIsbn.removeAll(Collections.singleton(null));
        List<BookDto> listBookDtoSearch = listBookSearchTitleAndIsbn.stream().map(this::convertToDto).collect(Collectors.toList());
        return listBookDtoSearch;
    }

    private Book convertToEntity(BookDto bookDto){
        ModelMapper mapper = new ModelMapper();
        Book book = mapper.map(bookDto,Book.class);
        return book;
    }

    private BookDto convertToDto(Book book) {
        ModelMapper mapper = new ModelMapper();
        BookDto bookDto = mapper.map(book, BookDto.class);
        return bookDto;
    }



}
