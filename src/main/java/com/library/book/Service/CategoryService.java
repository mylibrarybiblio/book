package com.library.book.Service;

import com.library.book.Dto.BookDto;
import com.library.book.Entity.Category;
import com.library.book.Exception.CategoryException;
import com.library.book.Repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public void addCategory(Category category) {
       Category categoryExist= categoryRepository.findByLabel(category.getLabel());
        if(categoryExist!= null) throw new CategoryException("Cette categorie existe déjà");
        categoryRepository.save(category);
    }

    public void updateCategory(Category category) {
        categoryRepository.findById(category.getIdCategory())
                .orElseThrow(()-> new CategoryException("Cette categorie n'existe pas"));
        categoryRepository.save(category);
    }

    public void deleteCategory(int idCategory) {
        Category category = categoryRepository.findById(idCategory)
                .orElseThrow(()-> new CategoryException("Cette categorie n'existe pas"));
        categoryRepository.delete(category);
    }

    public List<Category> getAllCategory() {
        List<Category> list = categoryRepository.findAll();
        return list;
    }

    public Category getOneCategory(int idCategory) {

        return categoryRepository.findById(idCategory).orElseThrow(()-> new CategoryException("Cette categorie n'existe pas"));
    }
}
