package com.library.book;

import com.library.book.Entity.Book;
import com.library.book.Entity.Category;
import com.library.book.Repository.BookRepository;
import com.library.book.Repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.List;

@SpringBootApplication
public class BookApplication implements CommandLineRunner {

	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private BookRepository bookRepository;

	public static void main(String[] args) {
		SpringApplication.run(BookApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		Category c1 = new Category("Programmation");
		Category c2 = new Category("Alogorithme");
		Category c3 = new Category("Systeme");
		categoryRepository.saveAll(List.of(c1,c2,c3));

		Book b1 = new Book("Java","ISBN 978–2-02-130453-4",new Date(),4,"MAMDY",c1);
		Book b2 = new Book("PHP","ISBN 778–2-02-130453-1",new Date(),4,"PCHARM",c1);
		Book b3 = new Book("ALGO","ISBN 978–2-02-130453-3",new Date(),4,"PETER",c2);
		bookRepository.saveAll(List.of(b1,b2,b3));
	}
}
