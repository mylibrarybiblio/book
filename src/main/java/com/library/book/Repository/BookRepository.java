package com.library.book.Repository;

import com.library.book.Entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
    Book findByIsbn(String isbn);
    List<Book> findByIsbnContainsIgnoreCase(String isbn);
    List<Book> findByTitleContains(String word);
}
